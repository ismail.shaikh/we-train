/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ~ Copyright 2020 Adobe Systems Incorporated
 ~
 ~ Licensed under the Apache License, Version 2.0 (the "License");
 ~ you may not use this file except in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~     http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing, software
 ~ distributed under the License is distributed on an "AS IS" BASIS,
 ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ~ See the License for the specific language governing permissions and
 ~ limitations under the License.
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

 const path = require('path');

 const BUILD_DIR = path.join(__dirname, 'dist');
 const CLIENTLIB_DIR = path.join(
   __dirname,
   '..',
   'ui.apps',
   'src',
   'main',
   'content',
   'jcr_root',
   'apps',
   'we-train',
   'clientlibs'
 );
 
 const libsBaseConfig = {
   allowProxy: true,
   serializationFormat: 'xml',
   cssProcessor: ['default:none', 'min:none'],
   jsProcessor: ['default:none', 'min:none']
 };
 
 
 
 const libsBaseMin = {
   allowProxy: true,
   serializationFormat: 'xml',
 };
 
 const defaulResources = {
   cwd: 'clientlib-<clib-name>', 
   files: ['**/*.*'], 
   flatten: false, 
   ignore: ['**/*.js', '**/*.css']
 }
 
 const generateClibs = (libsArr, libsBaseConfig, ...clObj) => {
  
   clObj.forEach((cl) => {
   let resources;
 
   let cssObj = {
     css: {
       cwd: `clientlib-${cl.name}`,
       files: ['**/*.css'],
       flatten: false
     }
   }
 
   let jsObj = {
     js: {
       cwd: `clientlib-${cl.name}`,
       files: ['**/*.js'],
       flatten: false
     }
   }
 
   let libsBase = libsBaseConfig
 
   if (cl.noCss) cssObj = {}
   if (cl.noJs) jsObj = {}
   if (cl.libsBaseConfig) libsBase = cl.libsBaseConfig
   else libsBase = libsBaseConfig
 
    if (cl.resources) {
      cl.resources.cwd = `clientlib-${cl.name}`
      resources = { resources: cl.resources }
    } else {
      resources = {}
    }
    
    let embed = cl.embed || []
 
    cl.components && cl.components.forEach((comp) => {
      const libsObj = {
        ...libsBase,
        name: `clientlib-${comp}`,
        categories: [`we-train.${comp}`],
        outputPath: path.join(COMP_DIR, cl.name, `clientlib-${comp}`),
        assets: {
          js: {
            cwd: `clientlib-${comp}`,
            files: ['**/*.js'],
            flatten: false
          },
          css: {
            cwd: `clientlib-${comp}`,
            files: ['**/*.css'],
            flatten: false
          },
        }
      }
 
      libsArr.push(libsObj)
      embed.push(libsObj.categories[0])
    })
 
    libsArr.push({
      ...libsBase,
      name: `clientlib-${cl.name}`,
      categories: [`we-train.${cl.name}`],
      embed: embed,
      dependencies: cl.dependencies ? cl.dependencies : [],
      assets: {
        // Copy entrypoint scripts and stylesheets into the respective ClientLib
        // directories
        ...jsObj,
        ...cssObj,
        ...resources
      }
    })
   })
 }
 
 
 
 const cliArray = [
   { name: 'dependencies' },
   { name: 'site', dependencies: ['we-train.dependencies'], resources: defaulResources },
   { name: 'base', embed: ['core.wcm.components.accordion.v1', 'core.wcm.components.tabs.v1', 'core.wcm.components.carousel.v1', 'core.wcm.components.image.v2', 'core.wcm.components.breadcrumb.v2', 'core.wcm.components.search.v1', 'core.wcm.components.form.text.v2', 'core.wcm.components.pdfviewer.v1', 'core.wcm.components.commons.datalayer.v1', 'we-train.grid'], resources: defaulResources, libsBaseConfig: libsBaseMin },
   { name: 'common' },
   { name: 'banner' },
   { name: 'heroHeader' },
   { name: 'smallerBanner' },
   { name: 'category' },
   { name: 'product' },
   { name: 'bigbanner' },
   { name: 'discountproduct' },
   { name: 'summerbanner' },
   { name: 'footer' },
   { name: 'customcarousel' },
   { name: 'extendedlist' },
   { name: 'teaserprimary' },
   { name: 'teaserprimaryreverse' },
   { name: 'teasersecondary' },
   { name: 'teasertertiary' },
   { name: 'smallerBannerfixed' },
   { name: 'mobileHeader' },
 ]
 
 
 const libsArr = []
 generateClibs(libsArr, libsBaseConfig, ...cliArray)
 
 // Config for `aem-clientlib-generator`
 module.exports = {
   context: BUILD_DIR,
   clientLibRoot: CLIENTLIB_DIR,
   libs: libsArr
 };
// var toggleButton = document.getElementById('toggleMenu');
// var mobileHeader = document.querySelector('.cmp-navigation--mobileheader');

// toggleButton.addEventListener('click', function () {
//     mobileHeader.style.opacity = 1;
//     mobileHeader.style.visibility = "visible";
//     mobileHeader.style.left = "0px";
//     mobileHeader.style.transition = "all 2s ease"
// });

// with

// var toggleButton = document.getElementById('toggleMenu');
// var mobileHeader = document.querySelector('.cmp-navigation--mobileheader');

// var isMobileHeaderVisible = false;

// toggleButton.addEventListener('click', function () {
//     if (!isMobileHeaderVisible) {
//         // Show the mobile header
//         mobileHeader.style.opacity = 1;
//         mobileHeader.style.visibility = "visible";
//         mobileHeader.style.left = "0px";
//     } else {
//         // Hide the mobile header
//         mobileHeader.style.opacity = 0;
//         mobileHeader.style.visibility = "hidden";
//         mobileHeader.style.left = "-275px";  // Assuming the width is 275px
//     }

//     // Toggle the visibility state
//     isMobileHeaderVisible = !isMobileHeaderVisible;

//     // Apply transition
//     mobileHeader.style.transition = "all 2s ease";
// });




// without two 
// var toggleButton = document.getElementById('toggleMenu');
// var mobileHeader = document.querySelector('.cmp-navigation--mobileheader');
// var isFlipped = false;

// toggleButton.addEventListener('click', function () {
//     mobileHeader.classList.toggle('cmp-navigation--mobileheaderShow');
//     isFlipped = !isFlipped;
//     toggleButton.style.transform = isFlipped ? 'scaleX(-1)' : 'scaleX(1)';
//     toggleButton.style.transition = "all 1s ease-in-out";
// });

// Without three
var toggleButton = document.getElementById('toggleMenu');
var mobileHeader = document.querySelector('.cmp-navigation--mobileheader');
var isFlipped = false;

// Function to handle clicks outside of toggle menu and mobile header
function handleClickOutside(event) {
    if (!toggleButton.contains(event.target) && !mobileHeader.contains(event.target)) {
        mobileHeader.classList.remove('cmp-navigation--mobileheaderShow');
        isFlipped = false;
        toggleButton.style.transform = 'scaleX(1)';
        document.removeEventListener('click', handleClickOutside);
    }
}

toggleButton.addEventListener('click', function () {
    mobileHeader.classList.toggle('cmp-navigation--mobileheaderShow');
    isFlipped = !isFlipped;
    toggleButton.style.transform = isFlipped ? 'scaleX(-1)' : 'scaleX(1)';
    toggleButton.style.transition = "all 1s ease-in-out";

    // Add click event listener to handle clicks outside toggle menu and mobile header
    document.addEventListener('click', handleClickOutside);
});


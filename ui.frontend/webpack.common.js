'use strict';

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TSConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const SOURCE_ROOT = __dirname + '/src/main/webpack';

const resolve = {
    extensions: ['.js', '.ts'],
    plugins: [new TSConfigPathsPlugin({
        configFile: './tsconfig.json'
    })]
};

module.exports = {
    resolve: resolve,
    entry: {
        site: SOURCE_ROOT + '/site/main.ts',
        common: SOURCE_ROOT + '/components/common/main.ts',
        banner: SOURCE_ROOT + '/components/e-commerceComponent/banner/main.ts',
        heroHeader: SOURCE_ROOT + '/components/e-commerceComponent/header/main.ts',
        mobileheader: SOURCE_ROOT + '/components/e-commerceComponent/mobileheader/main.ts',
        smallerBanner: SOURCE_ROOT + '/components/e-commerceComponent/smallerBanner/main.ts',
        category: SOURCE_ROOT + '/components/e-commerceComponent/category/main.ts',
        product: SOURCE_ROOT + '/components/e-commerceComponent/product/main.ts',
        bigbanner: SOURCE_ROOT + '/components/e-commerceComponent/bigbanner/main.ts',
        discountproduct: SOURCE_ROOT + '/components/e-commerceComponent/discountproduct/main.ts',
        summerbanner: SOURCE_ROOT + '/components/e-commerceComponent/summerbanner/main.ts',
        footer: SOURCE_ROOT + '/components/e-commerceComponent/footer/main.ts',
        customcarousel: SOURCE_ROOT + '/components/e-commerceComponent/customcarousel/main.ts',
        smallerBannerfixed: SOURCE_ROOT + '/components/e-commerceComponent/smallerBannerfixed/main.ts',
        extendedlist: SOURCE_ROOT + '/components/extendedlist/main.ts',
        teaserprimary: SOURCE_ROOT + '/components/teaserComponent/teaserprimary/main.ts',
        teaserprimaryreverse: SOURCE_ROOT + '/components/teaserComponent/teaserprimaryreverse/main.ts',
        teasersecondary: SOURCE_ROOT + '/components/teaserComponent/teasersecondary/main.ts',
        teasertertiary: SOURCE_ROOT + '/components/teaserComponent/teasertertiary/main.ts',
    },
    output: {
        filename: (chunkData) => {  
          return `clientlib-${chunkData.chunk.name}/[name].js`;
        },
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader'
                    },
                    {
                        loader: 'glob-import-loader',
                        options: {
                            resolve: resolve
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins() {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader',
                    },
                    {
                        loader: 'glob-import-loader',
                        options: {
                            resolve: resolve
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new ESLintPlugin({
            extensions: ['js', 'ts', 'tsx']
        }),
        new MiniCssExtractPlugin({
            filename: 'clientlib-[name]/[name].css'
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: path.resolve(__dirname, SOURCE_ROOT + '/resources'), to: './clientlib-site/' }
            ]
        })
    ],
    stats: {
        assetsSort: 'chunks',
        builtAt: true,
        children: false,
        chunkGroups: true,
        chunkOrigins: true,
        colors: false,
        errors: true,
        errorDetails: true,
        env: true,
        modules: false,
        performance: true,
        providedExports: false,
        source: false,
        warnings: true
    }
};

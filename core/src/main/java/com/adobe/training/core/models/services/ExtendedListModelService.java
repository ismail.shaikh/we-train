package com.adobe.training.core.models.services;

import com.adobe.cq.wcm.core.components.models.List;

import java.util.Map;

public interface ExtendedListModelService extends List {
    java.util.List<Object> customItemList();

}

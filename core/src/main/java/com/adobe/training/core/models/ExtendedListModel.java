package com.adobe.training.core.models;

import com.adobe.cq.wcm.core.components.models.List;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import com.adobe.training.core.models.services.CustomPropertiesListService;
import com.adobe.training.core.models.services.ExtendedListModelService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.osgi.service.component.annotations.Reference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
adapters = ExtendedListModelService.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExtendedListModel implements ExtendedListModelService {

    @OSGiService
    CustomPropertiesListService customPropertiesListService;
    @Self
    @Via(type = ResourceSuperType.class)
    private List list;

    @Override
    public Collection<ListItem> getListItems() {
        return list.getListItems();
    }

    @Override
    public boolean displayItemAsTeaser() {
        return list.displayItemAsTeaser();
    }

    @Override
    public boolean linkItems() {
        return list.linkItems();
    }

    @Override
    public boolean showDescription() {
        return list.showDescription();
    }

    @Override
    public boolean showModificationDate() {
        return list.showModificationDate();
    }

    @Override
    public String getDateFormatString() {
        return list.getDateFormatString();
    }

    @Override
    public String getId() {
        return list.getId();
    }

    @Override
    public  ComponentData getData() {
        return list.getData();
    }

    @Override
    public String getAppliedCssClasses() {
        return list.getAppliedCssClasses();
    }

    @Override
    public String getExportedType() {
        return list.getExportedType();
    }

    @Override
    public java.util.List<Object> customItemList() {
        Collection<ListItem> listItems=list.getListItems();
        Iterator<ListItem> iterator = listItems.iterator();
        java.util.List<Object> finalMap=customPropertiesListService.customPropertiesMap(iterator);
        return finalMap;
    }
}

package com.adobe.training.core.models.services;

import com.adobe.cq.wcm.core.components.models.Container;

public interface ExtendedContainerModelService extends Container {
    String backgroundImageTwo();
}

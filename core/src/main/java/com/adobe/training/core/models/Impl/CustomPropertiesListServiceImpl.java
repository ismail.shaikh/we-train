package com.adobe.training.core.models.Impl;

import com.adobe.cq.wcm.core.components.models.ListItem;
import com.day.cq.commons.jcr.JcrConstants;
import com.google.gson.JsonObject;
import com.adobe.training.core.models.services.CustomPropertiesListService;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.*;

@Component(service = CustomPropertiesListService.class, immediate = true)
public class CustomPropertiesListServiceImpl implements CustomPropertiesListService {
    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Override
    public List<Object> customPropertiesMap(Iterator<ListItem> iterator) {

        Map<String, Object> map = new HashMap<>();
        List<Object> finalList=new ArrayList<>();
        map.put(ResourceResolverFactory.SUBSERVICE, "subServiceName");
        try {
            ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(map);
            while (iterator.hasNext()) {
                ListItem listItem = iterator.next();
                String path = listItem.getPath();
                Node node=resourceResolver.getResource(path).adaptTo(Node.class);
                if (node.hasNode(JcrConstants.JCR_CONTENT)){
                    Map<String,String> finaObject=new HashMap<>();
                    Node jcrNode=node.getNode(JcrConstants.JCR_CONTENT);
                    String productCta=jcrNode.hasProperty("productCta") ? jcrNode.getProperty("productCta").getString() : "";
                    String productPrice=jcrNode.hasProperty("productprice") ? jcrNode.getProperty("productprice").getString() : "";
                    String productImage=jcrNode.hasProperty("productimg") ? jcrNode.getProperty("productimg").getString() : "";
                    String productIcon=jcrNode.hasProperty("productIcon") ? jcrNode.getProperty("productIcon").getString() : "";
                    String jcrTitle=jcrNode.hasProperty(JcrConstants.JCR_TITLE) ? jcrNode.getProperty(JcrConstants.JCR_TITLE).getString() : "";
                    String jcrDesc=jcrNode.hasProperty(JcrConstants.JCR_DESCRIPTION) ? jcrNode.getProperty(JcrConstants.JCR_DESCRIPTION).getString() : "";
                    finaObject.put("productCta",productCta);
                    finaObject.put("productprice",productPrice);
                    finaObject.put("productimg",productImage);
                    finaObject.put("productIcon",productIcon);
                    finaObject.put("title",jcrTitle);
                    finaObject.put("desc",jcrDesc);
                    finalList.add(finaObject);
                }
            }
            return finalList;
        } catch (LoginException e) {
            throw new RuntimeException(e);
        } catch (RepositoryException e) {
            throw new RuntimeException(e);
        }
    }
}
package com.adobe.training.core.models;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.wcm.core.components.commons.link.LinkManager;
import com.adobe.cq.wcm.core.components.models.Container;
import com.adobe.cq.wcm.core.components.models.ContainerItem;
import com.adobe.cq.wcm.core.components.models.datalayer.ComponentData;
import com.day.cq.wcm.api.designer.Style;
import com.drew.lang.annotations.Nullable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.adobe.training.core.models.services.ExtendedContainerModelService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.via.ResourceSuperType;

import java.util.List;
import java.util.Map;
import java.util.Objects;
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        adapters = ExtendedContainerModelService.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExtendedContainerModel implements ExtendedContainerModelService {
    @Self
    @Via(type = ResourceSuperType.class)
    private Container container;
    @JsonIgnore
    protected Style currentStyle;
    @Self
    protected LinkManager linkManager;

    @SlingObject
    protected Resource resource;
    String backImgCss="";
    @Override
    public String backgroundImageTwo() {
        return this.getBackgroundStyleTwo();
    }

    @Override
    public List<? extends ContainerItem> getChildren() {
        return container.getChildren();
    }

    @Override
    public Map<String, ? extends ComponentExporter> getExportedItems() {
        return container.getExportedItems();
    }

    @Override
    public String[] getExportedItemsOrder() {
        return container.getExportedItemsOrder();
    }

    @Override
    public String getId() {
        return container.getId();
    }

    @Override
    public ComponentData getData() {
        return container.getData();
    }

    @Override
    public String getAppliedCssClasses() {
        return container.getAppliedCssClasses();
    }

    @Override
    public String getExportedType() {
        return container.getExportedType();
    }
    private String getBackgroundImageTwo() {
        String image= String.valueOf(this.linkManager.get(this.resource).withLinkUrlPropertyName("backgroundImageReferenceTwo").build().getURL());
        backImgCss= String.valueOf(this.linkManager.get(this.resource).withLinkUrlPropertyName("backImg").build().getURL());
        return image;
    }
    public final @Nullable String getBackgroundStyleTwo() {
        StringBuilder styleBuilder = new StringBuilder();
        if (Objects.nonNull(this.getBackgroundImageTwo())){
            String image=this.getBackgroundImageTwo();
            styleBuilder.append("background:url(").append(image).append(")").append(backImgCss);
        }
        return styleBuilder.toString();
    }
    public final @Nullable String getBackgroundStyle() {
        StringBuilder styleBuilder = new StringBuilder();
        if (Objects.nonNull(this.getBackgroundImage()) && Objects.nonNull(this.getBackgroundImageTwo())){
            String image=this.getBackgroundImage();
            String imageTwo=this.getBackgroundImageTwo();
            styleBuilder.append("background-image:url(").append(image).append(")").append(",url("+imageTwo+")").append(backImgCss);
        }
        return styleBuilder.toString();
    }
    private String getBackgroundImage() {
        String image= String.valueOf(this.linkManager.get(this.resource).withLinkUrlPropertyName("backgroundImageReference").build().getURL());
        backImgCss= String.valueOf(this.linkManager.get(this.resource).withLinkUrlPropertyName("backImg").build().getURL());
        return image;
    }
}

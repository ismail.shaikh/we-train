package com.adobe.training.core.models.services;

import com.adobe.cq.wcm.core.components.models.ListItem;

import java.util.Iterator;
import java.util.List;

public interface CustomPropertiesListService {
    List<Object> customPropertiesMap(Iterator<ListItem> iterator);

}
